var express = require('express');
var router = express.Router();
var Cart = require('../models/cart');
var Product = require('../models/product');
var Order = require('../models/order');

// Get Homepage

router.get('/', function(req, res, next) {
    var successMsg = req.flash('success')[0];
    Product.find(function (err,docs) {
        var productChuncks =[];
        var chunkSize = 3;
        for(var i = 0; i < docs.length; i += chunkSize){
            productChuncks.push(docs.slice(i,i+chunkSize));
        }
        res.render('shop/index', { title: 'Shopping Cart', products:productChuncks, successMsg: successMsg, noMessages:!successMsg});

    });

});
router.get('/add-to-cart/:id',function(req,res,next){
    var productId =req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart:{});

    Product.findById(productId,function(err,product){
       if (err){
           return res.redirect('/');
       }
       cart.add(product, product.id);
        req.session.cart = cart;
        console.log(req.session.cart);
        res.redirect('/');
    });
});
router.get('/shopping-cart', function(req,res,next){
   if (!req.session.cart){
       return res.render('shop/shopping-cart',{products:null});
   }
   var cart = new Cart(req.session.cart);
    res.render('shop/shopping-cart',{products: cart.generateArray(),totalPrice:cart.totalPrice});
});
router.get('/checkout',isLoggedIn,function(req,res,next){
    if(!req.session.cart){
        return res.render('/shopping-cart');
    }
    var cart = new Cart(req.session.cart);
    var errMsg = req.flash('error')[0];
    res.render('shop/checkout',{total:cart.totalPrice, errMsg: errMsg, noError:!errMsg});
});
router.post('/checkout',isLoggedIn,function(req,res,next){
    if(!req.session.cart) {
        return res.redirect('/shopping-cart');
    }
    var cart = new Cart(req.session.cart);
    var stripe = require("stripe")(
        "sk_test_Jpyl3YPwhoOyO7bry2YHtPT9"
    );

    stripe.charges.create({
        amount: cart.totalPrice*100,
        currency: "usd",
        source: req.body.stripeToken, // obtained with Stripe.js
        description: "Test charge"
    }, function(err, charge) {
        if(err){
            req.flash('error',err.message);
            return res.redirect('/checkout');
        }
        var order = new Order({
            user:req.user,
            cart:cart,
            address:req.body.address,
            name:req.body.name,
            paymentId:charge.id

        });
        order.save(function (err,result) {
            req.flash('success','Successfully bought products');
            req.session.cart = null;
            res.redirect('/login');
        });

    });

});
module.exports = router;
function isLoggedIn(req, res, next){
    if(req.isAuthenticated()){
        return next();
    } else {
        req.session.oldUrl=req.url;
        req.flash('error_msg','You are not logged in');
        res.redirect('/users/login');
    }
}