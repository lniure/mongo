var Product = require('../models/product');
var mongoose = require('mongoose');
mongoose.connect('localhost:27017/loginapp');
var products = [
    new Product({
        imagePath:"http://localhost:3000/images/butterNaan.jpg",
        title:"Butter Naan",
        description:"Yummy Butter Naan",
        price:10
    }),
    new Product({
        imagePath:"http://localhost:3000/images/chickenTikkaMasala.jpg",
        title:"Chicken Tikka Masala",
        description:"Yummy Chicken Tikka Masala",
        price:20
    }),
    new Product({
        imagePath:"http://localhost:3000/images/ChickenManchurian.jpg",
        title:"Chicken Manchurian",
        description:"Yummy Chicken Manchurian",
        price:30
    }),
    new Product({
        imagePath:"http://localhost:3000/images/paneerTikka.jpg",
        title:"Paneer Tikka",
        description:"Yummy Paneer Tikka Masala",
        price:30
    }),
    new Product({
        imagePath:"http://localhost:3000/images/vegHotSoup.jpg",
        title:"Veg Soup",
        description:"Yummy Veggie Soup",
        price:30
    })];
var done=0;
for (var i=0; i<products.length;i++){
    products[i].save(function(err,result){
        done++;
        if(done === products.length){
            exit();
        }
    });
}
function exit(){
    mongoose.disconnect();
}

